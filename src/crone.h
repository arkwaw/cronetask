#pragma once

#include <string>
#include <set>
#include <vector>
#include <memory>

#include "tokenSpecification.h"
#include "operations/operation.h"

//todo everything here could be declared static
class Crone {
public:
    void run(std::string input, const std::vector<TokenSpecification>& tokensSpecifications);
    std::set<int> flatten(std::string token, const std::vector<std::unique_ptr<Operation>>& operations);
private:
    std::vector<std::string> split(const std::string& in, char delimiter);
    std::set<int> decodeBaseCase(const std::string& str);
};
