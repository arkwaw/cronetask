#include "everyNth.h"
#include <regex>

bool EveryNth::isValidOn(std::string str){
    const std::regex reg("[[*,0-9]+/[0-9]+");
    return regex_match(str, reg);
}

std::string EveryNth::apply(std::string str){
    if(str[0]=='*') str[0]='0';
    std::istringstream ss(str);

    char dash;
    int start,period;

    ss>>start>>dash>>period;

    std::string newString;

    for(int i = start ; i <= _cap ; i+=period){
        if(!newString.empty()) newString+=',';
        newString+=std::to_string(i);
    }
    return newString;
}
