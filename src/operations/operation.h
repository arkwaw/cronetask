#pragma once

#include <string>

class Operation{
public:
    virtual bool isValidOn(std::string str) = 0;
    virtual std::string apply(std::string str) = 0;
    virtual ~Operation(){};

};