#include <regex>
#include "range.h"

bool Range::isValidOn(std::string str){
    const std::regex reg("[[0-9]+-[0-9]+");
    return regex_match(str, reg);
}

std::string Range::apply(std::string str){
    std::string newString;
    std::istringstream ss(str);
    char dash;
    int start,end;
    ss>>start>>dash>>end;

    for(int i = start ; i <= end ; ++i){
        if(!newString.empty()) newString+=',';
        newString+=std::to_string(i);
    }

    return newString;
}