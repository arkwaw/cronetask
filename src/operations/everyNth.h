#pragma once

#include <string>
#include "operation.h"

class EveryNth : public Operation{
public:
    EveryNth(int cap): _cap(cap){}

    bool isValidOn(std::string str);
    std::string apply(std::string str);
private:
    int _cap;
};
