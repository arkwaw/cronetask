#pragma once
#include "operation.h"

class Everything : public Operation{
public:
    Everything(int start,int cap):_start(start),_cap(cap){}

    bool isValidOn(std::string str);
    std::string apply(std::string str);

private:
    int _start;
    int _cap;
};

