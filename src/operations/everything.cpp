#include "everything.h"

bool Everything::isValidOn(std::string str){
    return str=="*" && str.size()==1;
}

std::string Everything::apply(std::string str){
    std::string newString;
    for(int i = _start ; i <= _cap ; ++i){
        if(!newString.empty()) newString+=',';
        newString+=std::to_string(i);
    }

    return newString;
}
