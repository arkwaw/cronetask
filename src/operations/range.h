#pragma once
#include <string>

#include "operation.h"

class Range : public Operation{
public:
    bool isValidOn(std::string str);
    std::string apply(std::string str);
};