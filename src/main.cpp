﻿#include <iostream>
#include <vector>
#include <memory>

#include "operations/operation.h"
#include "operations/range.h"
#include "operations/everything.h"
#include "operations/everyNth.h"
#include "tokenSpecification.h"
#include "crone.h"

struct TokenRanges{
    int start;
    int end;
};

int main(int argc, char *argv[]) {
    TokenRanges minuteRange = {0,59};
    std::vector<std::unique_ptr<Operation>> minuteOperations;
    minuteOperations.emplace_back(std::make_unique<Range>());
    minuteOperations.emplace_back(std::make_unique<EveryNth>(minuteRange.end));
    minuteOperations.emplace_back(std::make_unique<Everything>(minuteRange.start,minuteRange.end));
    //TODO since we have common operations we could make some composition operation "Standard"

    TokenRanges hourRange = {0,59};
    std::vector<std::unique_ptr<Operation>> hourOperations;
    hourOperations.emplace_back(std::make_unique<Range>());
    hourOperations.emplace_back(std::make_unique<EveryNth>(hourRange.end));
    hourOperations.emplace_back(std::make_unique<Everything>(hourRange.start,hourRange.end));

    TokenRanges dayOfTheMonthRange = {1, 31};
    std::vector<std::unique_ptr<Operation>> dayOfTheMonthOperations;
    dayOfTheMonthOperations.emplace_back(std::make_unique<Range>());
    dayOfTheMonthOperations.emplace_back(std::make_unique<EveryNth>(dayOfTheMonthRange.end));
    dayOfTheMonthOperations.emplace_back(std::make_unique<Everything>(dayOfTheMonthRange.start,dayOfTheMonthRange.end));

    TokenRanges monthRange = {1, 12};
    std::vector<std::unique_ptr<Operation>> montOperations;
    montOperations.emplace_back(std::make_unique<Range>());
    montOperations.emplace_back(std::make_unique<EveryNth>(monthRange.end));
    montOperations.emplace_back(std::make_unique<Everything>(monthRange.start,monthRange.end));

    TokenRanges dayOfWeekRange = {1, 7};
    std::vector<std::unique_ptr<Operation>> dayOfTheWeekOperations;
    dayOfTheWeekOperations.emplace_back(std::make_unique<Range>());
    dayOfTheWeekOperations.emplace_back(std::make_unique<EveryNth>(dayOfWeekRange.end));
    dayOfTheWeekOperations.emplace_back(std::make_unique<Everything>(dayOfWeekRange.start,dayOfWeekRange.end));

    std::vector<TokenSpecification> tokensSpecifications;
    tokensSpecifications.emplace_back("minute",std::move(minuteOperations));
    tokensSpecifications.emplace_back("hour",std::move(hourOperations));
    tokensSpecifications.emplace_back("day of month",std::move(dayOfTheMonthOperations));
    tokensSpecifications.emplace_back("month",std::move(montOperations));
    tokensSpecifications.emplace_back("day of week",std::move(dayOfTheWeekOperations));


    Crone crone;
    if(argc > 1){
        crone.run(std::string(argv[1]), tokensSpecifications);
    }
    else{
        std::cout<<"Invalid number of arguments\n";
    }
  return 0;
}
