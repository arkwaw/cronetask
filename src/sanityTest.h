#pragma once
template <typename... T>
constexpr auto sumForSanity(const T &... args) {
  return (... + args);
}
