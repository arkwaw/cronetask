#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <iostream>
#include <memory>

#include "operations/range.h"
#include "operations/everything.h"
#include "operations/everyNth.h"
#include "crone.h"


void Crone::run(std::string input, const std::vector<TokenSpecification>& tokensSpecifications){
    auto tokens = split(input,' ');

    for(int i  = 0 ; i < tokens.size()-1 ; ++i){
        auto& specification = tokensSpecifications[i];

        auto numbers = flatten(
                tokens[i],
                specification.operations);

        //todo extract output
        auto padding = std::string(14-specification.name.size(),' ');
        std::cout<<specification.name<<padding;

        for(auto number : numbers) std::cout<<number<<" ";
        std::cout<<"\n";
    }

    std::cout<<"command: "<<tokens.back()<<"\n";
}

std::set<int> Crone::flatten(std::string token, const std::vector<std::unique_ptr<Operation>>& operations){

    for(auto& operation : operations){
        std::string intermediateToken="";
        for(auto t : split(token,',')){
            if(operation->isValidOn(t)){
                t=operation->apply(t);
            }
            if(intermediateToken!="") intermediateToken+=',';
            intermediateToken+=t;
        }
        token = intermediateToken;
    }

    return decodeBaseCase(token);
}

//todo this looks like a free function, not specific to the task
std::vector<std::string> Crone::split(const std::string& in, char delimiter){
    std::vector<std::string> tokens;
    std::istringstream ss(in);
    std::string token;

    while(getline(ss,token,delimiter)){
        tokens.push_back(token);
    }
    return tokens;
}
std::set<int> Crone::decodeBaseCase(const std::string& str){
    std::set<int> makeUnique;
    for(auto c : split(str,',')){
        makeUnique.insert(stoi(c));
    }
    return makeUnique;
}