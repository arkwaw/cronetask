#pragma once

#include <string>
#include <vector>
#include <memory>

#include "operations/operation.h"

class TokenSpecification {
public:
    TokenSpecification(std::string _name,
                       std::vector<std::unique_ptr<Operation>>&& _operations):
                       name(_name),
                       operations(std::move(_operations)){}
    std::string name;
    std::vector< std::unique_ptr<Operation> > operations;
};