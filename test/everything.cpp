#include "gtest/gtest.h"
#include "operations/everything.h"

TEST(everything, isValid) {
    Everything everything(0,59);
    EXPECT_TRUE(everything.isValidOn("*"));
    EXPECT_FALSE(everything.isValidOn("1-5"));
    EXPECT_FALSE(everything.isValidOn("*/55"));
}

TEST(everything, apply) {
    Everything everything(1,5);
    EXPECT_EQ("1,2,3,4,5", everything.apply("*"));
}