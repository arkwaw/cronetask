#include "gtest/gtest.h"
#include "operations/everyNth.h"

TEST(everyNth, isValid) {
    EveryNth everything(59);
    EXPECT_TRUE(everything.isValidOn("*/15"));
    EXPECT_FALSE(everything.isValidOn("1-5"));
}

TEST(everyNth, apply) {
    EveryNth everything(59);
    EXPECT_EQ("0,20,40", everything.apply("*/20"));
    EXPECT_EQ("5,25,45", everything.apply("5/20"));
}