#include "gtest/gtest.h"
#include "operations/range.h"

TEST(range, isValid) {
    Range range;
    EXPECT_TRUE(range.isValidOn("0-3"));
    EXPECT_FALSE(range.isValidOn("1,5"));
}

TEST(range, apply) {
    Range range;
    EXPECT_EQ("0,1,2,3", range.apply("0-3"));
    EXPECT_EQ("1,2,3,4,5", range.apply("1-5"));
}