cmake_minimum_required (VERSION 3.10.3)

include(GoogleTest)

set(SOURCE_FILES
        sanityTest.cpp
        ../src/operations/range.cpp
        ../src/operations/range.h
        ../src/operations/everyNth.cpp
        ../src/operations/everyNth.h
        ../src/operations/everything.cpp
        ../src/operations/everything.h
        range.cpp
        everything.cpp
        everyNth.cpp
        ../src/tokenSpecification.cpp
        ../src/tokenSpecification.h
        ../src/crone.cpp
        ../src/crone.h
        crone.cpp)

add_executable(tests
    ${SOURCE_FILES}
)

target_include_directories(tests 
    PUBLIC 
        ../src
        ../lib/benchmark/googletest/googletest/include
)

set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

target_link_libraries(tests 
    PUBLIC 
        gtest
        gtest_main
)

gtest_discover_tests(tests)
