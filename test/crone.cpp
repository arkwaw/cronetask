#include <set>
#include <vector>

#include "gtest/gtest.h"
#include "crone.h"

#include "operations/range.h"
#include "operations/everything.h"
#include "operations/everyNth.h"

TEST(crone, flatenRange) {
Crone crone;
std::vector<std::unique_ptr<Operation>> operations;
operations.emplace_back(std::make_unique<Range>());
EXPECT_EQ(std::set<int>({1,2,3,4,5,6,7}), crone.flatten("1-5,2-7", operations));
}

TEST(crone, flatenEverything) {
Crone crone;
std::vector<std::unique_ptr<Operation>> operations;
operations.emplace_back(std::make_unique<Everything>(1,5));
EXPECT_EQ(std::set<int>({1,2,3,4,5}), crone.flatten("*,*", operations));
}

TEST(crone, flatenEveryNth) {
Crone crone;
std::vector<std::unique_ptr<Operation>> operations;
operations.emplace_back(std::make_unique<EveryNth>(50));
EXPECT_EQ(std::set<int>({3,5,18,20,33,35,48,50}), crone.flatten("5/15,3/15", operations));
}

TEST(crone, flatenMix) {
Crone crone;
std::vector<std::unique_ptr<Operation>> operations;
operations.emplace_back(std::make_unique<Range>());
operations.emplace_back(std::make_unique<EveryNth>(50));

EXPECT_EQ(std::set<int>({1,2,3,4,5,7,20,35,50}), crone.flatten("5/15,7,1-4", operations));
}